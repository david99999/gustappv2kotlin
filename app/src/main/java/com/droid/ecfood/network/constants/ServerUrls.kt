package com.superchat.network.constants

/**
 * Created by david on 24/02/16.
 */

const val SERVER_URL = "http://prod.gustapp.com/v1/"
const val LOGIN = "user/login"
const val USER = "user/{userId}"
const val CHAT_ROOM = "chatRooms"
const val CHAT_THREAD = "chatRooms/{chatRoomId}"
const val CHAT_MESSAGE = "chatRooms/{chatRoomId}/message"