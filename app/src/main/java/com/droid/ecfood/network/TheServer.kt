package com.mymvc.presenter.interfaces.communications

import android.content.Context
import com.droid.ecfood.BuildConfig
import com.superchat.network.constants.SERVER_URL
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by david on 11/02/16.
 */
interface TheServer {


    companion object {

        lateinit var instance: TheServer

        fun init(context: Context, url: String = SERVER_URL) {
            var builder = OkHttpClient().newBuilder()

            if (BuildConfig.DEBUG) {
                var interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY;
                builder.addInterceptor(interceptor);
            }
            builder.cache(Cache(context.cacheDir, 10 * 1024 * 1024)) //10Mb

            val restAdapter = Retrofit.Builder()
                    .baseUrl(url)
                    .client(builder.build())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            instance = restAdapter.create(TheServer::class.java)
        }
    }
}