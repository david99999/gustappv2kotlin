package com.droid.ecfood.features.login

import android.os.Bundle
import com.droid.ecfood.R
import com.droid.ecfood.features.FacebookActivity

class LoginActivity : FacebookActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }
}
