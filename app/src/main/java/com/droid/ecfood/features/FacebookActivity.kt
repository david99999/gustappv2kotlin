package com.droid.ecfood.features

import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import com.droid.ecfood.utils.AnalyticsUtils
import com.droid.ecfood.utils.GeneralConstants
import com.droid.ecfood.utils.Utils
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.FacebookSdk
import com.facebook.share.Sharer
import com.facebook.share.widget.ShareDialog
import com.google.android.gms.maps.model.LatLng

import fr.quentinklein.slt.LocationTracker
import fr.quentinklein.slt.TrackerSettings
import hugo.weaving.DebugLog

open class FacebookActivity : AppCompatActivity() {

    interface GustappLocationListener {
        fun onLocationDetected()
    }

    internal var listener: GustappLocationListener? = null
    private var shareCallback: com.facebook.FacebookCallback<com.facebook.share.Sharer.Result>? = object : FacebookCallback<Sharer.Result> {
        override fun onSuccess(result: Sharer.Result) {

        }

        override fun onCancel() {

        }

        @DebugLog
        override fun onError(e: FacebookException) {
        }
    }

    fun getCallbackManager(): CallbackManager {
        return callbackManager
    }

    internal var callbackManager: CallbackManager = CallbackManager.Factory.create()
    var shareDialog: ShareDialog? = null

    internal var settings: TrackerSettings = TrackerSettings()
            .setUseGPS(true)
            .setUseNetwork(true)
            .setUsePassive(true)
            .setTimeBetweenUpdates((60 * 1000).toLong()) //cada  minuto
            .setMetersBetweenUpdates(200f)

    internal var currenLocation: Location? = null
    internal var tracker: LocationTracker? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FacebookSdk.sdkInitialize(applicationContext)
        shareDialog = ShareDialog(this)
        // this part is optional
        shareDialog!!.registerCallback(getCallbackManager(), shareCallback)
        AnalyticsUtils.sendScreenView()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }


    @DebugLog
    internal fun startLocation() {
        try {
            tracker = object : LocationTracker(this, settings) {
                override fun onLocationFound(location: Location) {
                    // Do some stuff when a new location has been found.
                    currenLocation = location
                    Utils.saveValueInPrefs(this@FacebookActivity, com.droid.ecfood.utils.GeneralConstants.LastLatitude, location.latitude.toString())
                    Utils.saveValueInPrefs(this@FacebookActivity, com.droid.ecfood.utils.GeneralConstants.LastLongitude, location.longitude.toString())

                    if (listener != null)
                        listener!!.onLocationDetected()
                    stopLocation()
                }


                override fun onTimeout() {
                }
            }
        } catch (e: Exception) {
        }

    }

    @DebugLog
    internal fun stopLocation() {
        try {
            if (tracker != null) tracker!!.stopListen()
        } catch (e: Exception) {
        }

    }

    val position: LatLng
        get() {

            if (currenLocation == null) {
                if (Utils.containKeyInPrefs(this, GeneralConstants.LastLatitude)) {
                    return LatLng(java.lang.Double.parseDouble(Utils.getKeyFromPrefs(this, GeneralConstants.LastLatitude)),
                            java.lang.Double.parseDouble(Utils.getKeyFromPrefs(this, GeneralConstants.LastLongitude)))
                } else {
                    return LatLng(0.0, 0.0)
                }
            }

            return LatLng(currenLocation!!.latitude, currenLocation!!.longitude)
        }

    override fun onDestroy() {
        super.onDestroy()
        stopLocation()
        shareCallback = null
        shareDialog = null
        tracker = null
        currenLocation = null
    }
}
