package com.droid.ecfood.utils;

/**
 * Created by David on 13/08/2014.
 */
public final class GeneralConstants {

	public final static String MY_PREFS_FILE_NAME = "GustappPrefs";

	public final static String FBToken                     = "facebookToken";
	public final static String IsForUpdate                 = "IsForUpdate";
	public final static String GustappToken                = "GustappToken";
	public final static String UserLikesRegistered         = "UserLikesRegistered";
	public final static String IngredientCategory          = "category";
	public final static String CompanyId                   = "company_id";
	public final static String LocationId                  = "location_id";
	public final static String MenuGroupId                 = "meugroup_id";
	public final static String RecommendedAlreadyDisplayed = "RecommendedAlreadyDisplayed";
	public final static String LoggedBy                    = "LoggedBy";
	public final static String LoggedByFace                = "LoggedByFace";
	public final static String LoggedByMail                = "LoggedByMail";

	public final static String IsFromRecommended = "IsFromRecommended";

	public final static String INFO_DIALOG_TITLE          = "titulo";
	public final static String INFO_DIALOG_DESC           = "descripcion";
	public final static String INFO_DIALOG_ACCEPT_CAPTION = "aceptar";

	public final static String Added   = "add";
	public final static String Removed = "delete";


	public final static String ShouldRefreshRecommended = "ShouldRefreshRecommended";


	public final static String PointTo     = "PointTo";
	public final static String Production  = "Production";
	public final static String Development = "Development";

	public static final String EXTRA_MESSAGE                    = "message";
	public static final String PROPERTY_REG_ID                  = "registration_id";
	public final static int    PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	public static final String PROPERTY_APP_VERSION             = "appVersion";
	/**
	 * Substitute you own sender ID here. This is the project number you got
	 * from the API Console, as described in "Getting Started."
	 */
	//public final static String SENDER_ID = "597421709625";//"AIzaSyCQ6ESJTPvKHjwaPkpYVYW48TQ7SmuMcFM"; https://console.developers.google.com/project/597421709625/apiui/credential?authuser=0
	public final static String SENDER_ID                        = "1044070304329";//"AIzaSyBnH02Fa3TBVt6ydm_TcWDmvN09Hb-ABAU"; https://console.developers.google.com/project/1044070304329/apiui/credential?authuser=0
	/**
	 * Tag used on log messages.
	 */
	public static final String TAG                              = "GCMGustapp";
	public static final String GUSTAPP_LOG                      = "GUSTAPP_LOG";

	public static final String FACEBOOK_ERROR    = "FACEBOOK_ERROR";
	public static final String PLAY_SERVICES     = "PlayServices";
	public static final String LOCATION_PROVIDER = "LocationProvider";
	public static final String GENERAL_EXCEPTION = "GeneralException";
	public static final String MAP_PINS          = "pines";
	public static final String GENERAL_INFO      = "GeneralInfo";
	public static final String RETROFIT_ERROR    = "RetrofitError";
	public static final String DistanceValue = "DistanceValue";
	public static final String RecommendedOnlyNear = "RecommendedOnlyNear";
	public static final String LastKnownLocation = "LastKnownLocation";
	public static final String LastLatitude = "LastLatitude";
	public static final String LastLongitude = "LastLongitude";
	public static final String BranchName = "BranchName";
	public static final String IS_FOR_LOCATION = "IS_FOR_LOCATION";
}
