package com.droid.ecfood.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.droid.ecfood.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import static com.droid.ecfood.R.anim.anim_scale_down;
import static com.droid.ecfood.R.anim.anim_scale_up;
import static com.droid.ecfood.R.anim.mini_anim_scale_down;
import static com.droid.ecfood.R.anim.mini_anim_scale_up;

/**
 * Created by David on 16/07/2014.
 */
public class Utils {

    public static void LogFacebookKeySash(Context context) {
        // Add code to print out the key hash
        try {
            PackageInfo info = context.getPackageManager()
                    .getPackageInfo(context.getApplicationContext()
                            .getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash","KeyHash:" + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            Log.e("KeyHash",e.getMessage());
        }
    }

    public static void makeImageViewDarkOnClick(View imageView) {
        //set the ontouch listener
        imageView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageView view = (ImageView) v;
                        //overlay is black with transparency of 0x77 (119)
                        view.getDrawable()
                                .setColorFilter(0x35000000, PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                        ImageView view2 = (ImageView) v;
                        //clear the overlay
                        view2.getDrawable()
                                .clearColorFilter();
                        view2.invalidate();
                        break;

                    case MotionEvent.ACTION_CANCEL: {
                        ImageView view = (ImageView) v;
                        //clear the overlay
                        view.getDrawable()
                                .clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }

                return false;
            }
        });
    }

    public static void makeImageViewScaleOnClick(final View imageView) {

        imageView.setOnTouchListener(new View.OnTouchListener() {
            final Animation _animUp = AnimationUtils.loadAnimation(imageView.getContext(), anim_scale_up);
            final Animation _animDown = AnimationUtils.loadAnimation(imageView.getContext(), anim_scale_down);

            @Override
            public boolean onTouch(final View view, MotionEvent event) {
                switch (event.getAction()) {

                    case (MotionEvent.ACTION_DOWN):
                        view.startAnimation(_animDown);
//                        Vibrator v = (Vibrator) imageView.getContext().getSystemService(Context.VIBRATOR_SERVICE);
//                        v.vibrate(25);
                        view.invalidate();
                        return false;
                    case (MotionEvent.ACTION_UP):
                        view.startAnimation(_animUp);
                        view.invalidate();
                        return false;
                    case (MotionEvent.ACTION_CANCEL):
                        view.startAnimation(_animUp);
                        view.invalidate();
                        return false;
                }
                return false;
            }
        });
    }

    public static void makeImageViewMiniScaleOnClick(final View imageView) {

        imageView.setOnTouchListener(new View.OnTouchListener() {
            final Animation _animUp = AnimationUtils.loadAnimation(imageView.getContext(), mini_anim_scale_up);
            final Animation _animDown = AnimationUtils.loadAnimation(imageView.getContext(), mini_anim_scale_down);

            @Override
            public boolean onTouch(final View view, MotionEvent event) {
                switch (event.getAction()) {

                    case (MotionEvent.ACTION_DOWN):
                        view.startAnimation(_animDown);
//                        Vibrator v = (Vibrator) imageView.getContext().getSystemService(Context.VIBRATOR_SERVICE);
//                        v.vibrate(25);
                        view.invalidate();
                        return false;
                    case (MotionEvent.ACTION_UP):
                        view.startAnimation(_animUp);
                        view.invalidate();
                        return false;
                    case (MotionEvent.ACTION_CANCEL):
                        view.startAnimation(_animUp);
                        view.invalidate();
                        return false;
                }
                return false;
            }
        });
    }

    private static BitmapDrawable highlightImage(Context context, Bitmap src) {
        // create new bitmap, which will be painted and becomes result image
        Bitmap bmOut = Bitmap.createBitmap(src.getWidth() + 5, src.getHeight() + 5, Bitmap.Config.ARGB_8888);
        // setup canvas for painting
        Canvas canvas = new Canvas(bmOut);
        // setup default color
        canvas.drawColor(0, PorterDuff.Mode.CLEAR);
        // create a blur paint for capturing alpha
        Paint ptBlur = new Paint();
        ptBlur.setMaskFilter(new BlurMaskFilter(15, BlurMaskFilter.Blur.NORMAL));
        int[] offsetXY = new int[2];
        // capture alpha into a bitmap
        Bitmap bmAlpha = src.extractAlpha(ptBlur, offsetXY);
        // create a color paint
        Paint ptAlphaColor = new Paint();
        ptAlphaColor.setColor(0xFFFFFFFF);
        // paint color for captured alpha region (bitmap)
        canvas.drawBitmap(bmAlpha, offsetXY[0], offsetXY[1], ptAlphaColor);
        // free memory
        bmAlpha.recycle();

        // paint the image source
        canvas.drawBitmap(src, 0, 0, null);
        // return out final image
        return new BitmapDrawable(context.getResources(), bmOut);
    }

    public static void makeImageViewHighlightOnClick(Context context, final ImageView imageView) {
        final BitmapDrawable Original = ((BitmapDrawable) imageView.getDrawable());
        final BitmapDrawable Highlighted = highlightImage(context, ((BitmapDrawable) imageView.getDrawable()).getBitmap());
        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(final View view, MotionEvent event) {
                switch (event.getAction()) {

                    case (MotionEvent.ACTION_DOWN):
                        imageView.setImageDrawable(Highlighted);
                        return false;
                    case (MotionEvent.ACTION_UP):
                        imageView.setImageDrawable(Original);
                        return false;
                    case (MotionEvent.ACTION_CANCEL):
                        imageView.setImageDrawable(Original);

                }
                return false;
            }
        });

    }

    public static void saveValueInPrefs(Context context, String key, String token) {
        //        final SharedPreferences prefs = new ObscuredSharedPreferences(getActivity(), getActivity().getSharedPreferences(GeneralConstants.MY_PREFS_FILE_NAME, Context.MODE_PRIVATE));
        //
        //        prefs.edit().putString(key, token).commit();
        //        prefs.getString(GeneralConstants.FBToken, null);
        // Put (all puts are automatically committed)
        getPrefs(context).put(key, token);
        // Get
        //        String user = preferences.getString("userId");

    }

    public static boolean containKeyInPrefs(Context context, String key) {
        return getPrefs(context).containsKey(key);
    }

    public static void removeKeyFromPrefs(Context context, String key) {
        getPrefs(context).removeValue(key);
    }

    public static String getKeyFromPrefs(Context context, String key) {
        return getPrefs(context).getString(key);
    }

    protected static SecurePreferences getPrefs(Context context) {
        return new SecurePreferences(context, GeneralConstants.MY_PREFS_FILE_NAME, "T@nÇ€Re", true);
    }




    /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/*::                                                                         :*/
/*::  This routine calculates the distance between two points (given the     :*/
/*::  latitude/longitude of those points). It is being used to calculate     :*/
/*::  the distance between two locations using GeoDataSource (TM) prodducts  :*/
/*::                                                                         :*/
/*::  Definitions:                                                           :*/
/*::    South latitudes are negative, east longitudes are positive           :*/
/*::                                                                         :*/
/*::  Passed to function:                                                    :*/
/*::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :*/
/*::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :*/
/*::    unit = the unit you desire for results                               :*/
/*::           where: 'M' is statute miles                                   :*/
/*::                  'K' is kilometers (default)                            :*/
/*::                  'N' is nautical miles                                  :*/
/*::  Worldwide cities and other features databases with latitude longitude  :*/
/*::  are available at http://www.geodatasource.com                          :*/
/*::                                                                         :*/
/*::  For enquiries, please contact sales@geodatasource.com                  :*/
/*::                                                                         :*/
/*::  Official Web site: http://www.geodatasource.com                        :*/
/*::                                                                         :*/
/*::           GeoDataSource.com (C) All Rights Reserved 2014                :*/
/*::                                                                         :*/
/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

    public static double calculateDistance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515 * 1.609344;
        return (dist);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/*::  This function converts decimal degrees to radians             :*/
/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/*::  This function converts radians to decimal degrees             :*/
/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

    public static void showNextActivityFromRight(Activity activity) {
        activity.overridePendingTransition(R.anim.show_activity_from_right, R.anim.current_activity_hide);
    }

    public static void hideCurrentActivityByRight(Activity activity) {
        activity.overridePendingTransition(R.anim.previous_activity_show_again, R.anim.hide_activity_by_right);
    }

    public static void showNextActivityFromBottom(Activity activity) {
        activity.overridePendingTransition(R.anim.show_activity_from_bottom, R.anim.current_activity_hide);
    }

    public static void hideCurrentActivityByBottom(Activity activity) {
        activity.overridePendingTransition(R.anim.previous_activity_show_again, R.anim.hide_activity_by_bottom);
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getBounds()
                .width(), drawable.getBounds()
                .height(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static String formatTextForPrice(String price) {
        try {
            DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
            simbolo.setDecimalSeparator(',');
            simbolo.setGroupingSeparator('.');
            DecimalFormat formateador = new DecimalFormat("###,###", simbolo);
            return formateador.format(Integer.parseInt(price));
//            NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
//            return format.format(Integer.parseInt(price));

        } catch (Exception e) {
            return price;
        }
    }

    public static void unbindDrawables(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup && !(view instanceof AdapterView)) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }

    public static boolean isGPSEnabled(Context context) {
        LocationManager service = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return service.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }
}
