package com.droid.ecfood.utils

import android.app.Activity
import android.content.Context

import com.droid.ecfood.BuildConfig
import com.droid.ecfood.R
import com.google.android.gms.analytics.GoogleAnalytics
import com.google.android.gms.analytics.HitBuilders
import com.google.android.gms.analytics.Tracker
import com.mymvc.presenter.interfaces.communications.TheServer
import com.superchat.network.constants.SERVER_URL

import hugo.weaving.DebugLog
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by ByYuto on 11/11/2015.
 */
class AnalyticsUtils {

    //direccion de la consola del proyecto https://console.developers.google.com/apis/credentials?project=gustapp2014

    val SCREEN_RECOMMENDED = "SCREEN_RECOMMENDED"
    val SCREEN_RESTAURANTS_GUIDE = "SCREEN_RESTAURANTS_GUIDE"
    val SCREEN_RESTAURANT_MENU_GROUP = "SCREEN_RESTAURANT_MENU_GROUP"
    val SCREEN_RESTAURANT_PRODUCTS = "SCREEN_RESTAURANT_PRODUCTS"
    val SCREEN_RESTAURANT_BRANCH_INFO = "SCREEN_RESTAURANT_BRANCH_INFO"

    val CATEGORY_RESTAURANT_DISPLAYED = "CATEGORY_RESTAURANT_DISPLAYED"
    val CATEGORY_BRANCH_DISPLAYED = "CATEGORY_BRANCH_DISPLAYED"

    fun sendRestaurantDisplayed(restaurantId: String) {
        if (!BuildConfig.DEBUG)
            sendEvent(CATEGORY_RESTAURANT_DISPLAYED, restaurantId)
    }

    fun sendBranchDisplayed(restaurantId: String, branchName: String) {
        if (!BuildConfig.DEBUG)
            sendEvent(CATEGORY_BRANCH_DISPLAYED, restaurantId, branchName)
    }

    @DebugLog
    private fun sendEvent(category: String, action: String, label: String) {
        instance.send(HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).setValue(1).build())
    }

    @DebugLog
    private fun sendEvent(category: String, action: String) {
        instance.send(HitBuilders.EventBuilder().setCategory(category).setAction(action).setValue(1).build())
    }


    companion object {

        lateinit var instance: Tracker

        fun init(context: Context) {
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            instance = GoogleAnalytics.getInstance(context).newTracker(R.xml.app_tracker)
        }

        @DebugLog
        fun sendScreenView() {
            //getCurrentTracker(context).setScreenName(screenName);
            instance.send(HitBuilders.ScreenViewBuilder().build())
        }
    }


}