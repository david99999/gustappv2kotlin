package com.superchat.app

/**
 * Created by david on 23/02/16.
 */
import android.app.Application
import com.droid.ecfood.utils.AnalyticsUtils
import com.mymvc.presenter.interfaces.communications.TheServer


class GustApp : Application() {

    override fun onCreate() {
        super.onCreate()
        TheServer.init(this)
        AnalyticsUtils.init(this)
    }

}